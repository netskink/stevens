//
//  wraplib.c
//  stevens
//
//  Created by John Fred Davis on 3/17/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

// Wrapper function for our own library functions
// Most are included in the source file for the function itself


#include "unp.h"
#include "error.h"

void Inet_pton(int family, const char *strptr, void *addrptr) {
    
    int n;
    
    if ( (n = inet_pton(family, strptr, addrptr)) < 0) {
        err_sys("inet_pton error for %s", strptr);      // errno set
    } else if (n == 0) {
        err_quit("inet_pton error for %s", strptr);     // errno not set
    }
    
    // nothing to return
}
