//
//  wrapunix.c
//  stevens
//
//  Created by John Fred Davis on 3/15/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//


#include "unp.h"
#include "error.h"
#include "wrapunix.h"


void *Calloc(size_t n, size_t size) {
    void	*ptr;
    
    if ( (ptr = calloc(n, size)) == NULL) {
        err_sys("calloc error");
    }
    
    return(ptr);
}


void Close(int fd) {
    
    if (close(fd) == -1) {
        err_sys("close error");
    }
}

pid_t Fork(void) {
    pid_t pid;
    
    if ( (pid = fork()) == -1) {
        err_sys("fork error");
    }
    return (pid);
}

int Ioctl(int fd, int request, void *arg) {
    int		n;
    
    if ( (n = ioctl(fd, request, arg)) == -1) {
        err_sys("ioctl error");
    }
    return(n);	/* streamio of I_LIST returns value */
}

void * Malloc(size_t size) {
    void	*ptr;
    
    ptr = malloc(size);
    if (NULL == ptr) {
        err_sys("malloc error");
    }
    return(ptr);
}


void Write(int fd, void *ptr, size_t nbytes) {

    if (write(fd, ptr, nbytes) != nbytes) {
        err_sys("write error");
    }
}