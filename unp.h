//
//  unp.h
//  SimpleApp
//
//  Created by John F. Davis on 3/13/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#ifndef unp_h
#define unp_h

/////////////
// out of order
#include <stdlib.h>  // For exit()


#include <net/route.h>
#include <net/if.h>
#include <net/if_dl.h>



///////////////

#include "config.h"           // my hand written config.h

#include <sys/types.h>          // basic system data types
#include <sys/socket.h>         // basic socket definitions
#include <sys/time.h>           // timeval{} for select()
                                // stuff omitted
#include <netinet/in.h>         // sockaddr_in{} and other internet defns
#include <arpa/inet.h>          // inet(3) functions, inet_pton(),

#include <errno.h>

#include <stdio.h>

// Stuff omitted - from line 20 start
#include <string.h>
#include <sys/stat.h>       // For S_xxx file mode constants
#include <sys/uio.h>        // for iovect{} and readv/writev
#include <unistd.h>         // for read()
#include	<sys/wait.h>
#include	<sys/un.h>		/* for Unix domain sockets */



// Stuff omitted - from line 53 start
#include <pthread.h>

// Stuff omitted - from line 83 start
#include <sys/ioctl.h>


// Stuff omitted - from line 140 start
#define MAXLINE ( (unsigned long) 4096)        // max text line length
#define BUFFSIZE ( (unsigned long)   8192)    // buffer size for reads and writes


// Stuff omitted - from line 147 start
// Following shortens all the typecasts of pointer arguments
#define SA struct sockaddr


// Stuff omitted - from line 193 start
// Following could be dervied from SOMAXCONN in <sys/socket.h> but many kernels still
// define it as 5 while actually supporting many more
#define LISTENQ ( (unsigned long) 1024)  // second arg to listen()


// Stuff omitted - from line 200 start
// Define some port number that can be used for our examples
#define SERV_PORT ( (unsigned long) 9877)   // tcp and udp


// Stuff omitted - from line 291 start
ssize_t  readline(int, void *, size_t);


// Stuff omitted - from line 413
// Prototypes for our own library wrapper functions
void Inet_pton(int, const char *, void *);

// Stuff omitted - from line 467
/* prototypes for our stdio wrapper functions: see {Sec errors} */
//void     Fclose(FILE *);
//FILE    *Fdopen(int, const char *);
char    *Fgets(char *, int, FILE *);
//FILE    *Fopen(const char *, const char *);
void     Fputs(const char *, FILE *);

// Stuff omitted - from line 495
ssize_t  Readline(int, void *, size_t);


// prototypes for our socket wrapper functions
void Connect(int, const SA *, socklen_t);

// prototype for ntop from line 424
char	*Sock_ntop(const SA *, socklen_t);
char	*Sock_ntop_host(const SA *, socklen_t);



// stuff omitted - from line 509
void Writen(int, void *, size_t);


#define min(a,b)    ((a) < (b) ? (a) : (b))
#define max(a,b)    ((a) > (b) ? (a) : (b))

#endif /* unp_h */
