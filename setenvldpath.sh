#!/bin/bash

# This sets ld library path to the current directory.
# Lets see if we can set the path based upon the current directory.
# ie, if we are in /tmp lets add /tmp to the executable path
# and the DYLD_LIBRARY_PATH

# export DYLD_LIBRARY_PATH=.

# Get the current directory

CURRENT_DIR="$(pwd)"
export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$CURRENT_DIR
echo ${CURRENT_DIR} added to DYLD_LIBRARY_PATH environment variable
echo "DYLD_LIBRARY_PATH="$DYLD_LIBRARY_PATH


export PATH=$PATH:$CURRENT_DIR
echo ${CURRENT_DIR} added to PATH environment variable
echo "PATH="$PATH
