//
//  wrapsock.h
//  stevens
//
//  Created by John Fred Davis on 3/15/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#ifndef wrapsock_h
#define wrapsock_h

int Accept(int fd, struct sockaddr *sa, socklen_t *salenptr);

void Bind(int fd, const struct sockaddr *sa, socklen_t salen);

void Connect(int fd, const struct sockaddr *sa, socklen_t salen);


void Listen(int fd, int backlog);


int Socket(int family, int type, int protocol);


#endif /* wrapsock_h */
