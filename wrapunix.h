//
//  wrapunix.h
//  stevens
//
//  Created by John Fred Davis on 3/15/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#ifndef wrapunix_h
#define wrapunix_h

void *Calloc(size_t n, size_t size);
void Close(int fd);
pid_t Fork(void);
int Ioctl(int fd, int request, void *arg);
void *Malloc(size_t size);
void Write(int fd, void *ptr, size_t nbytes);


#endif /* wrapunix_h */
