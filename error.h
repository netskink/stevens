//
//  error.h
//  stevens
//
//  Created by John Fred Davis on 3/15/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#ifndef error_h
#define error_h



void err_sys(const char *fmt, ...);

// Fatal error unrelated to system call
// print message and terminate

void err_quit(const char *fmt, ...);


void err_ret(const char *fmt, ...);

#endif /* error_h */
