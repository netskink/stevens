//
//  wrapsock.c
//  stevens
//
//  Created by John Fred Davis on 3/15/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#include "unp.h"
#include "error.h"
#include "wrapsock.h"


int Accept(int fd, struct sockaddr *sa, socklen_t *salenptr) {

    int n;

again:
    if ( (n = accept(fd, sa, salenptr)) < 0) {
        if (errno == ECONNABORTED) {
            goto again;
        } else {
            err_sys("accept error");
        }
    }
    return (n);
}


void Bind(int fd, const struct sockaddr *sa, socklen_t salen) {
    
    if (bind(fd, sa, salen) < 0) {
        err_sys("bind error");
    }
    
}

void Connect(int fd, const struct sockaddr *sa, socklen_t salen) {
    
    if (connect(fd, sa, salen) < 0) {
        err_sys("bind error");
    }
}

void Listen(int fd, int backlog) {
    
    char *ptr;
    
    // 4can override 2nd arg with environment variable
    if ( (ptr = getenv("LISTENQ")) != NULL) {
        backlog = atoi(ptr);
    }
    
    if (listen(fd, backlog) < 0) {
        err_sys("Listen error");
    }
}

int Socket(int family, int type, int protocol) {
    
    int n;
    
    if ( (n= socket(family, type, protocol)) < 0) {
        err_sys("socket error");
    }
    return n;
}
