//
//  wrapstdio.c
//  stevens
//
//  Created by John Fred Davis on 3/26/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#include "unp.h"
#include "error.h"


char *Fgets(char *ptr, int n, FILE *stream) { 
    char    *rptr;
    
    if ( (rptr = fgets(ptr, n, stream)) == NULL && ferror(stream)) {
        err_sys("fgets error");
    }
    
    return (rptr);
}

void Fputs(const char *ptr, FILE *stream)
{
    if (fputs(ptr, stream) == EOF)
        err_sys("fputs error");
}